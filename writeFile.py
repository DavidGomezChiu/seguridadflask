import os, errno, re
cwd = os.getcwd()

def writefile (action,name,text,origen,borrar,destino):
    if action == "cifrar":
        name = name[:-4] + ".cfr.txt"
    else:
        name = name[:-8] + ".txt"
    if (origen == ""):
        originDirectory = cwd
    else:
        if (origen[-1] == '/'):
            origen = origen[0:-1]
        originDirectory = origen
    if (destino == ""):
        destinationDirectory = cwd+os.path.sep
    else:
        if (destino[-1] == '/'):
            destino = destino[0:-1]
        destinationDirectory = destino+os.path.sep
    try:
        f = open(destinationDirectory+name,"w+")
        f.write(text)
        f.close()
    except OSError as e:
        print("No se pudo escribir el archivo:\n",e)
    if borrar == "True":
        erasefile(originDirectory)

def erasefile(name):
    try:
        os.remove(name)
    except OSError as e:
        print("No se pudo borrar el archivo",e)