from flask import Flask, render_template, request
import writeFile
import json

app = Flask(__name__)

#app.debug = True

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/cifra-texto")
def cifraTexto():
    return render_template('cifraTexto.html')

@app.route("/descifra-texto")
def descifraTexto():
    return render_template('descifraTexto.html')

@app.route("/cifra-archivo")
def cifraArchivo():
    return render_template('cifraArchivo.html')

@app.route("/descifra-archivo")
def descifraArchivo():
    return render_template('descifraArchivo.html')

@app.route("/write", methods=["POST"])
def write():
    action = request.form.get('action')
    name = request.form.get('name')
    text = request.form.get('text')
    origen = request.form.get('origen')
    borrar = request.form.get('borrar')
    destino = request.form.get('destino')
    writeFile.writefile(action,name,text,origen,borrar,destino)
    return ""

if __name__ == "__main__":
    app.run()